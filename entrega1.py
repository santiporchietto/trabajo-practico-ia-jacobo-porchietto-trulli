from simpleai.search import SearchProblem
from simpleai.search.traditional import breadth_first, depth_first, limited_depth_first, iterative_limited_depth_first, uniform_cost, greedy, astar
from simpleai.search.viewers import WebViewer, ConsoleViewer, BaseViewer

#Lista con posibles movimientos de los barcos piratas: arriba, abajo, derecha, izquierda
Movimientos = [(1,0), (-1,0), (0,1), (0,-1)]
#Posicion isla francesa
IslaFrancesa = (2,0)
#Posicion isla pirata
IslaPirata = (5,5)
#Tamaño del tablero (arista del cuadrado)
TamanioTablero = 6


#Funcion que comprueba si una posicion es valida, es decir si esta dentro de los limites del tablero
def comprobarPosicionValida(posicion):
	return posicion[0] >= 0 and posicion[0] < TamanioTablero and posicion[1] >= 0 and posicion[1] < TamanioTablero

#Funcion que calcula la distancias que existe entre dos posiciones (en casilleros) 
def manhatan(pos1, pos2):
	return abs(pos1[0] - pos2[0]) + abs(pos1[1] - pos2[1])

class entrega1(SearchProblem):
	
	def actions(self, state):
		acciones = []
		barcosPiratas = state[1]

		#Por cada barco pirata agrego a la lisat de acciones aquellas que den como resultado una posicion valida
		for ibarco, barco in enumerate(barcosPiratas):
			#barco es una tupla que cuenta con:
			#	barco[0]: una tupla (x,y) donde x es la fila, y la columna. Representa la posicion del barco
			#	barco[1]: 1 o 0 que indica si el barco tiene o no el mapa del tesoro respectivamente
			for movimiento in Movimientos:
				if comprobarPosicionValida((movimiento[0] + barco[0][0], movimiento[1] + barco[0][1])):
					#La accion es una tupla que tiene el movimiento, y que barco pirata lo tiene que hacer
					acciones.append((movimiento, ibarco))
		
		return acciones


	def result(self, state, action):
		listState = [list(state[0]), list(state[1])]

		accion, ibarco = action

		barco = state[1][ibarco]

		nuevaPosicion = (barco[0][0] + accion[0], barco[0][1] + accion[1])

		if nuevaPosicion in listState[0]:
			#Si la nueva posicion coincide con uno de los barcos franceses, 
			#elimino a los dos barcos de sus respectivas listas
			listState[0].remove(nuevaPosicion)
			listState[1].pop(ibarco)
		elif nuevaPosicion == IslaFrancesa:
			#Si la nueva posicion es la isla francesa, actualizo el barco pirata con su nueva posicion
			#y cargo el mapa (1 en la posicion 1 de la tupla)
			listState[1][ibarco] = (nuevaPosicion, 1)
		else:
			#Si no se da ninguna de las condiciones anteriores, actualizo el barco pirata con su nueva posicion
			listState[1][ibarco] = (nuevaPosicion, barco[1])

		return (tuple(listState[0]), tuple(listState[1]))


	def cost(self, state, action, state2):
		#El costo siempre es 1, que corresponde al movimiento de un barco pirata
		return 1


	def is_goal(self, state):
		#Comprueba si en la lista de barcos piratas (state[1]) existe alguno que este en la pisicion de la
		#isla pirata y tenga cargado un mapa del tesoro ((IslaPirata, 1))
		return (IslaPirata, 1) in state[1]


	def heuristic(self, state):
		#Si el barco pirata tiene mapa: Distancia del barco a la isla pirata
		#Si el barco pirata no tiene mapa: Distancia del barco a la isla francesa mas distancia entre islas
		#La heuristica devuelve el minimo de todos los calculos
		distanciaEntreIslas = manhatan(IslaPirata, IslaFrancesa)
		minDistancia = 100
		for barco in state[1]:
			if barco[1] == 1:
				distanciaIslaPirataBarco = manhatan(barco[0], IslaPirata)
				if distanciaIslaPirataBarco < minDistancia:
					minDistancia = distanciaIslaPirataBarco
			else:
				distanciaIslaFrancesaBarco = manhatan(barco[0], IslaFrancesa)
				if distanciaIslaFrancesaBarco + distanciaEntreIslas < minDistancia:
					minDistancia = distanciaIslaFrancesaBarco + distanciaEntreIslas

		return minDistancia


def resolver(metodo_busqueda, franceses, piratas):
	viewer = None
	barcos = []

	for barco in piratas:
		barcos.append((barco,0))

	initial = (tuple(franceses),tuple(barcos))

	problem = entrega1(initial)

	if metodo_busqueda == "breadth_first":    
		resultado = breadth_first(problem, graph_search=True, viewer = viewer)
	elif metodo_busqueda == "greedy":    
		resultado = greedy(problem, graph_search=True, viewer = viewer)
	elif metodo_busqueda == "depth_first":    
		resultado = depth_first(problem, graph_search=True, viewer = viewer)
	elif metodo_busqueda == "astar":
		resultado = astar(problem, graph_search=True, viewer = viewer)
	elif metodo_busqueda == "uniform_cost":
		resultado = uniform_cost(problem, graph_search=True, viewer = viewer)

	return resultado


if __name__ == '__main__':
	"""
	#viewer = WebViewer()
	#viewer = ConsoleViewer()
	viewer = None
	#viewer = BaseViewer()

	#metodo = "greedy"
	metodo = "breadth_first"
	#metodo = "astar"
	#metodo = "uniform_cost"
	#metodo = "depth_first"

	franceses = [(0,2), (0,3), (1,2), (1,3), (2,1), (2,2), (2,3), (3,0), (3,1), (3,2), (4,0), (4,1), (5,0)]
	piratas = [(4,4), (4,5), (5,4)]

	result = resolver(metodo, franceses, piratas)

	print("#" * 80)
	print("Franceses:")
	print(franceses)
	print("Piratas:")
	print(piratas)
	print("#" * 80)
	print("Estado final: {}".format(result.state))
	print("#" * 80)
	for accion, resultado in enumerate(result.path()):
		print('Accion',accion, ':', resultado[0])
		print('Resultado:', resultado[1])

	print("#" * 80)
	print("Nodos Visitados: {}".format(viewer.stats['visited_nodes']))
	print("Profundidad solucion: {}".format(len(result.path())))
	print("Costo: {}".format(result.cost))
	print("Tamaño máximo frontera: {}".format(viewer.stats['max_fringe_size']))
	"""