import itertools
from simpleai.search import (CspProblem, backtrack, min_conflicts,
                             MOST_CONSTRAINED_VARIABLE,
                             LEAST_CONSTRAINING_VALUE,
                             HIGHEST_DEGREE_VARIABLE)
from simpleai.search.csp import _find_conflicts
from datetime import datetime

Charlas ={
1: "Django Girls",#: requiere de computadoras, y se espera una asistencia de 40 personas
2: "Introducción a Python",#: requiere de computadoras, y debería darse por la mañana
3: "Keynote Diversidad",#: requiere un aula de al menos 150 personas, proyector y sistema de audio. Debería ser dada por la tarde. Y no se deben dar otras charlas en el mismo horario que esta
4: "Keynote Core Developer",#: igual que la keynote anterior
5: "APIs Rest",#: requiere proyector.
6: "Diseño Sistemas Accesibles",#: no puede ser en la planta alta.
7: "Unit Testing",#: requiere proyector.
8: "Editores de Código",#: requiere proyector y sistema de audio
9: "Música Python",#: requiere proyector, más de 60 personas.
10: "Software, negocios, contabilidad y mucho más",#: no debería ocupar el aula magna. Además debe ser por la mañana
11: "Análisis de Imágenes",#: no puede darse en el laboratorio.
12: "Satélites Espaciales",#: requiere proyector, y darse por la tarde
13: "Lib PyPI",#: requiere proyector.
14: "Introducción a Pandas",#: requiere proyector, solo puede darse en la planta alta.
}

Aulas = {
1: "Magna",#: capacidad para 200 personas, en la planta baja, y posee proyector y sistema de audio.
2: "42",#: capacidad para 100 personas, en la planta alta, posee proyector pero no equipo de audio.
3: "Laboratorio",#: capacidad para 50 personas, en la planta baja, no proyector ni equipo de audio, pero dispone de computadoras para los asistentes
}

Horarios = [10, 11, 14, 15, 16, 17]

def generar_problema_entrega2():

	variables = [charla for key, charla in Charlas.items()]

	dominios = { variable: [] for variable in variables}

	dominios[Charlas[1]] = [(Aulas[3], horario) for horario in Horarios]
	dominios[Charlas[2]] = [(Aulas[3], horario) for horario in Horarios if horario < 12]
	dominios[Charlas[3]] = [(Aulas[1], horario) for horario in Horarios if horario > 12]
	dominios[Charlas[4]] = [(Aulas[1], horario) for horario in Horarios if horario > 12]
	dominios[Charlas[5]] = [(aula, horario) for key, aula in Aulas.items() for horario in Horarios if key != 3]
	dominios[Charlas[6]] = [(aula, horario) for key, aula in Aulas.items() for horario in Horarios if key != 2]
	dominios[Charlas[7]] = [(aula, horario) for key, aula in Aulas.items() for horario in Horarios if key != 3]
	dominios[Charlas[8]] = [(Aulas[1], horario) for horario in Horarios]
	dominios[Charlas[9]] = [(aula, horario) for key, aula in Aulas.items() for horario in Horarios if key != 3]
	dominios[Charlas[10]] = [(aula, horario) for key, aula in Aulas.items() for horario in Horarios if key != 1 and horario < 12]
	dominios[Charlas[11]] = [(aula, horario) for key, aula in Aulas.items() for horario in Horarios if key != 3]
	dominios[Charlas[12]] = [(aula, horario) for key, aula in Aulas.items() for horario in Horarios if key != 3 and horario > 12]
	dominios[Charlas[13]] = [(aula, horario) for key, aula in Aulas.items() for horario in Horarios if key != 3]
	dominios[Charlas[14]] = [(Aulas[2], horario) for horario in Horarios]


	restricciones = []

	for var1, var2 in itertools.combinations(variables, 2):
		restricciones.append(((var1, var2), diferentes))

	for var in variables:
		if var != Charlas[3]:
			restricciones.append(((Charlas[3], var), unica_en_horario))
		if var != Charlas[4]:
			restricciones.append(((Charlas[4], var), unica_en_horario))

	return CspProblem(variables, dominios, restricciones)


def diferentes(vars, vals):
	return vals[0] != vals[1]

def unica_en_horario(vars, vals):
	return vals[0][1] != vals[1][1]

def resolver(metodo_busqueda, iteraciones):
	problema = generar_problema_entrega2()

	if metodo_busqueda == "backtrack":
		resultado = backtrack(problema)

		#resultado = backtrack(problema, variable_heuristic=MOST_CONSTRAINED_VARIABLE, value_heuristic=LEAST_CONSTRAINING_VALUE)
	elif metodo_busqueda == "min_conflicts":    
		resultado = min_conflicts(problema, iterations_limit=iteraciones)

	return resultado

if __name__ == '__main__':
	"""metodo = "backtrack"
	iteraciones = None

	#metodo = "min_conflicts"
	#iteraciones = 50

	inicio = datetime.now()
	resultado = resolver(metodo, iteraciones)
	print("tiempo {}".format((datetime.now()-inicio).total_seconds()))
	print(resultado)
	print(repr(resultado))
	
	problema = generar_problema_entrega2()
	conflictos = _find_conflicts(problema, resultado)
	print("Numero de conflictos en la solucion: {}".format(len(conflictos)))"""